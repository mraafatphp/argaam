@extends('site.layouts.main')
@section('content')
<div class="row">

  <!-- Blog Entries Column -->
  <div class="col-md-6">

    <h1 class="my-4">{{ trans('main.articles') }}</h1>

    @foreach($articles as $article)
    <!-- Blog Post -->
    <div class="card mb-4">
      <img class="card-img-top" src="{{ route('viewimage', ['image_name' => $article->image]) }}" alt="Card image cap">
      <div class="card-body">
        <a href="{{ route('viewArticle', ['id' => $article->id]) }}"><h2 class="card-title">{{ $article->title }}</h2></a>
      </div>
    </div>
    @endforeach



  </div>

  <!-- Sidebar  -->
  <div class="col-md-6">
    <h1 class="my-4">{{ trans('main.featured_articles') }}</h1>
    @foreach($featured_articles as $article)
    <article>
      <div class="row">
          <div class="col-sm-6 col-md-6">
              <figure>
                  <img src="{{ route('viewimage', ['image_name' => $article->image]) }}">
              </figure>
          </div>
          <div class="col-sm-6 col-md-6">
                <a href="{{ route('viewArticle', ['id' => $article->id]) }}"><h4>{{ $article->title }}</h4></a>
          </div>
      </div>
    </article>
    @endforeach
  </div>

</div>
<!-- /.row -->
@endsection
