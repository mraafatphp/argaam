
@extends('site.layouts.main')
@section('content')
<div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{ $article->title }}</h1>

        <hr>

        <!-- Date/Time -->
        <p>{{ $article->created_at }}</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{ route('viewimage', ['image_name' => $article->image]) }}" alt="">

        <hr>

        {!! str_replace('IMAGE_URL', url('image') ,$article->content) !!}


      </div>


    </div>
    @endsection
