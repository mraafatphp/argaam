@if (session('status') == 'added')
            <div class="alert alert-success">
                <b>تمت الإضافة بنجاح</b>
            </div>
            @elseif (session('status') == 'updated')
            <div class="alert alert-success">
                <b>تم التعديل بنجاح</b>
            </div>
            @elseif (session('status') == 'deleted')
            <div class="alert alert-danger">
                <b>تم الحذف بنجاح</b>
            </div>
                 @elseif (session('status') == 'published_before')
            <div class="alert alert-danger">
                <b>تم النشر بالفعل من قبل</b>
            </div>
        @endif
