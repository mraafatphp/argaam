<!DOCTYPE html>
<html>
<head>
	<title>{{ trans('main.login') }}</title>
</head>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<body>
  <div class="container" style="margin-top:30px">
  <div class="col-md-12">
      <div class="modal-dialog" style="margin-bottom:0">
          <div class="modal-content">
                      <div class="panel-heading">
                          <h3 class="panel-title">{{ trans('main.login') }}</h3>
                      </div>
                      <div class="panel-body">
                          <form role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
														@if (Session::has('error'))
															 <div class="alert alert-danger">
																	 <span>{{ Session::get('error') }}</span>
															 </div>
													 @endif
                              <fieldset>
                                  <div class="form-group">
                                      <input class="form-control" placeholder="{{ trans('main.email') }}" name="email" type="email" autofocus="">
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" placeholder="{{ trans('main.password') }}" name="password" type="password" value="">
                                  </div>
                                  <button class="btn btn-sm btn-success">{{ trans('main.login') }}</button>
                              </fieldset>
                          </form>
                      </div>
                  </div>
      </div>
  </div>

  </div>

</body>
</html>
