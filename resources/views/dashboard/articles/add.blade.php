@extends('dashboard.layouts.main')
@section('content')
<div class="row">
       <div class="col-md-12">
           <h4 class="page-header">{{ trans('main.add') }}</h4>
       </div>
   </div>
   <div class="row">
       <form action="{{ route('add_article_post') }}" method="post" enctype="multipart/form-data">
           {{ csrf_field() }}
           <div class="col-md-8">

               <div class="form-group">
                   <label>{{ trans('main.title') }}:</label>
                   <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                   <p class="help-block"><b class="text-info titleCount"></b></p>
                   @if($errors->first('title'))
                       <p class="help-block"><b class="text-danger">{{ $errors->first('title') }}</b></p>
                   @endif
               </div>
               <div class="form-group">
                   <label>{{ trans('main.content') }}:</label>
                   <textarea id="tinymce-content" class="content" name="content">{{ old('content') }}</textarea>
                   @if($errors->first('content'))
                       <p class="help-block"><b class="text-danger">{{ $errors->first('content') }}</b></p>
                   @endif
               </div>

           </div>
           <div class="col-md-4">
               <div class="form-group">
                   <label>{{ trans('main.section') }}:</label>
                   <ul class="list-unstyled">
                     @foreach($sections as $section)
                     <li><label><input type="checkbox" class="section" name="section_id" value="{{ $section->id }}" @if(old('section_id') == $section->id) checked @elseif(old('section_id') && old('section_id') != $section->id) disabled @endif> {{ $section->name }} </label></li>
                     @endforeach
                  </ul>
                   @if($errors->first('section_id'))
                       <p class="help-block"><b class="text-danger">{{ $errors->first('section_id') }}</b></p>
                   @endif
               </div>

               <!-- Main Image -->
               <div class="form-group">
                 <label>{{ trans('main.image') }}</label>
                 <input type="file" name="image" onchange="loadFile(event)">
                   @if($errors->first('image'))
                       <p class="help-block"><b class="text-danger">{{ $errors->first('image') }}</b></p>
                   @endif
               </div>
               <img id="preview_output" class="img-responsive" style="max-width: 100%;margin-top: 10px;"/>
               <!-- End of Main Image -->
           </div>
   </div>

   <div class="row">
       <div class="form-group">
           <button class="btn btn-lg btn-primary">{{ trans('main.save') }}</button>
       </div>
   </div>
   </form>

@endsection
@section('script')
<script src="{{ asset('/admin/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
           });
        $('.section').click(function(){
            var inputs = $('.section');
            if($(this).is(':checked')){
                inputs.not(this).prop('disabled',true);
            }else{
              inputs.not(this).prop('disabled',false);
            }
       });
    });

    tinymce.init({
        selector: 'textarea#tinymce-content',
        height: 500,
        menubar: false,
        directionality: "rtl",
        language: "ar",
        // paste_as_text: true,
        convert_urls : 0,
        paste_data_images: true,
        plugins: [
            'advlist autolink lists link image code charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: 'insert | image code | undo redo | paste |  formatselect | bold italic forecolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | insertImage | insertVideo | insertEmbed',
        images_upload_url: "{{ route('upload_inner_image') }}",
        images_upload_handler: function (blobInfo, success, failure) {
            $.post("{{ route('upload_inner_image') }}", {image: blobInfo}, function(data){
                console.log(data.location);
                success(data.location);
                //  var img = '<img src="'+ data.location +'">';
                // tinymce.activeEditor.insertContent(img);
            }, 'JSON');
        },
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '{{ asset("/admin/css/tinymce.min.css") }}'],
        setup: function(editor) {

        }
    });


    // Create Image Thumb preview
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function(){
            var output = document.getElementById('preview_output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };


</script>
@endsection
