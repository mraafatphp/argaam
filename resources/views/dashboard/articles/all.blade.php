@extends('dashboard.layouts.main')
@section('content')

    <div class="row">
        <div class="col-md-12">
          <h1 class="page-header">{{ trans('main.articles') }}</h1>
        </div>
    </div>
    <div class="row">
                @include('dashboard.partials.alerts')
      <div class="table-responsive">
      <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>{{ trans('main.title') }}</th>
        <th>{{ trans('main.view') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($articles as $article)
      <tr>
        <td>{{ $article->id }}</td>
        <td>{{ $article->title }}</td>
        <td>
          <a href="{{ route('viewArticle', ['id' => $article->id]) }}" target="_blank" class="btn btn-info btn-sm">{{ trans('main.view') }}</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
    </div>
@endsection
@section('script')

@endsection
