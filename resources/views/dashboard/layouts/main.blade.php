<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('/admin/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/sb-admin-2.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/metisMenu.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/jquery-ui.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('/admin/css/custom.css') }}" rel="stylesheet">

  <script src="{{ asset('/admin/js/jquery-1.11.0.js') }}"></script>
  <script src="{{ asset('/admin/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('/admin/js/fieldChooser.min.js') }}"></script>
  <script src="{{ asset('/admin/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/admin/js/metisMenu.min.js') }}"></script>
  <script src="{{ asset('/admin/js/sb-admin-2.js') }}"></script>

    <title>News DashBoard</title>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="mainLogo" href="{{ route('admin_home') }}">
                    <img src="{{ asset('admin/imgs/logo.png')  }}" />
                </a>
            </div>
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ route('admin_home') }}"><i class="fa fa-home fa-fw"></i>{{ trans('main.home') }}</a>
                        </li>



                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i>{{ trans('main.articles') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                  <a href="{{ route('add_article') }}">{{ trans('main.add') }}</a>
                                </li>
                                <li>
                                  <a href="{{ route('all_articles') }}">{{ trans('main.all') }}</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        <a href="{{ route('featured_articles') }}"><i class="fa fa-home fa-fw"></i>{{ trans('main.featured_articles') }}</a>
                        </li>


                        <li>
                            <a href="{{ route('logout') }}"><i class="fa fa-lock fa-fw"></i>{{ trans('main.logout') }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
             @yield('content')

        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
@yield('script')
</body>

</html>
