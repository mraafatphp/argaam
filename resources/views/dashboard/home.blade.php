@extends('dashboard.layouts.main')
@section('content')

    <div class="row">
        <div class="col-md-6">
          <h2>{{ trans('main.articles') }}</h2>
          <h3>{{ $articles }}</h3>
        </div>
        <div class="col-md-6">
          <h2>{{ trans('main.featured_articles') }}</h2>
          <h3>{{ $featured_articles }}</h3>
        </div>

    </div>
@endsection
@section('script')

@endsection
