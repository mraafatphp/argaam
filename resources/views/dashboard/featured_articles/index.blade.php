@extends('dashboard.layouts.main')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">{{ trans('main.featured_articles') }}</h1>
        </div>
    </div>
                @include('dashboard.partials.alerts')
      <div class="row">
<!--
    <div class="form-inline">
      <div class="form-group">
        <input type="text" class="form-control" id="search" placeholder="ابحث">
      </div>
      <button type="button" class="btn btn-primary" id="searchButton">عرض نتائج البحث</button>
      <button type="button" class="btn btn-info">عرض الكل</button>
      <hr />

    </div> -->
            <div id="fieldChooser" tabIndex="1">
            <div id="sourceFields">
              @foreach($articles as $article)
                <div><h5>{{ $article->title }}<h5>
                  <input type="hidden" name="featured_articles[]" value="{{ $article->id }}">
                </div>
              @endforeach
            </div>
            <form id="saveForm" method="post" action="{{ route('handle_featured_articles') }}">
              {{ csrf_field() }}
            <div id="destinationFields">
              @if(count($current_articles) > 0)
              @foreach($current_articles as $current_article)
              <div class="fc-field fc-selected" tabindex="1"><h5>{{ $current_article->title }}</h5><h5>
                  <input type="hidden" name="featured_articles[]" value="{{ $current_article->id }}">
             </div>
              @endforeach
              @endif
            </div>
            </form>
        </div>
    </div>
    <br />
      <div class="row">
        <div class="form-group">
          <button form="saveForm" type="submit" class="btn btn-lg btn-primary col-md-12">{{ trans('main.save') }}</button>
        </div>
      </div>
      <br />
      <br />
@endsection
@section('script')
<script>
  $(document).ready(function () {
      var $sourceFields = $("#sourceFields");
      var $destinationFields = $("#destinationFields");
      var $chooser = $("#fieldChooser").fieldChooser(sourceFields, destinationFields);
  });
  </script>
@endsection
