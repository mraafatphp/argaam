<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Article;
use Image;
class ArticlesController extends MainController
{


    // Get All Articles
    public function showAllArticles(){
      $articles = Article::orderBy('id', 'desc')->get();
      return view('dashboard.articles.all', compact('articles'));
    }

    // Get Add Artcile View
    public function addArticle(){
      $sections = Section::get();
      return view('dashboard.articles.add', compact('sections'));
    }

    // Handle Add Article
    public function handleAddArticle(Request $request){
      // validation
      $validate = $request->validate([
         'title'             => 'required|min:15',
         'content'           => 'required|min:50',
         'section_id'        => 'required',
         'image'        => 'required',
     ]);
     // get inputs
     $inputs = $request->except('_token', 'image');
     // replace image path in content
     $inputs['content'] = str_replace(url('image'), 'IMAGE_URL', $inputs['content']);

     // Check And Upload Image
     if($request->hasFile('image')){
       $imageName = uniqid() . rand() . '.jpg';
       Image::make($request->image)
           ->encode('jpg', 100)
           ->save(public_path('/uploads/') . $imageName);
       $inputs['image'] = $imageName;
     }
     // Store data into databae
      if (Article::create($inputs)) {
           return redirect()->route('all_articles')->with('status', 'added');
       }

    }

    // Upload Inner Image
  public function uploadInnerImage(){
      $imageName = uniqid() . rand() . '.jpg';
      Image::make(request()->image['base64'])
          ->resize(800, null, function ($constraint) {
              $constraint->aspectRatio();
          })
          ->encode('jpg', 100)
          ->save(public_path('/uploads/') . $imageName);
      return json_encode(['location' => route('viewimage' ,['image_name' => $imageName])]);
  }
}
