<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\FeaturedArticle;
use DB;
class FeaturedArticlesController extends MainController
{
    // Get featured articles view
    public function index(){
      $articles = $this->articles()->get();
      $current_articles = $this->featuredArticles()->get();


      return view('dashboard.featured_articles.index', compact('articles', 'current_articles'));
    }

    // Handle featured articles
    public function handle(Request $request){
      $info = $request->input('featured_articles');

      // Delete Old
      FeaturedArticle::truncate();
      if (!empty($info)) {
          $all_articles = [];
          foreach ($info as $key => $value) {

              $all_articles[] = ['article_id' => $value];
          }
          //dd($all_articles);
          FeaturedArticle::insert($all_articles);
      }
      return redirect()->route('featured_articles')->with('status', 'updated');

      }
}
