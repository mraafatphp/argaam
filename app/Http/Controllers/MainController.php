<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use DB;
class MainController extends Controller
{
    protected function articles(){
      $articles = Article::whereNotIn('id', DB::table('featured_articles')
              ->pluck('article_id'))
              ->orderBy('id', 'desc');
      return $articles;
    }

    protected function featuredArticles(){
      $featured_articles = Article::join('featured_articles', 'featured_articles.article_id', '=', 'articles.id')
      ->select('articles.*', 'featured_articles.id as feature_id')
      ->orderBy('feature_id', 'asc');
      return $featured_articles;
    }

}
