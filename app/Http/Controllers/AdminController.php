<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class AdminController extends MainController
{

    // Get login view
    public function showLoginForm(){
     return view('dashboard.auth.login');
    }

    // Handle Login
    public function login(Request $request){
       $validate = $request->validate([
           'email' => 'required',
           'password' => 'required'
       ]);

       $email = $request->input('email');
       $password = $request->input('password');

       if (Auth::attempt(['email' => $email, 'password' => $password])) {
           // Authentication passed...
           return redirect()->intended('dashboard');
       }else{
            return redirect()->back()->with('error', trans('main.user_not_found'));
        }
    }
    // Handle Logout
    public function logout(){
       Auth::logout();
       return redirect()->route('login');

    }

    // Admin Home

    public function home(){
      $articles = $this->articles()->count();
      $featured_articles = $this->featuredArticles()->count();
      return view('dashboard.home', compact('articles', 'featured_articles'));
    }
}
