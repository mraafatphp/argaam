<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
class HomeController extends MainController
{

  // View Image
  public function viewImage($image_name)
    {
      $remoteImage = public_path('/uploads/'. $image_name);
      if (file_exists($remoteImage)) {
          $imginfo = getimagesize($remoteImage);
          header("Content-type: {$imginfo['mime']}");
          return readfile($remoteImage);
      } else {
          return "Not Found";
      }
    }

    // home Page
    public function home(){
      $articles = $this->articles()->get();
      $featured_articles = $this->featuredArticles()->get();
      return view('site.home.home', compact('featured_articles', 'articles'));
    }

    // View Article
    public function viewArticle($id){
      $article = Article::findOrFail($id);
      return view('site.home.article', compact('article'));
    }
}
