<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HomePage
Route::get('/', 'HomeController@home')->name('home');
Route::get('/article/{id}', 'HomeController@viewArticle')->name('viewArticle');
Route::get('/image/{image_name}', 'HomeController@viewImage')->name('viewimage');

// DashBoard
Route::group(['prefix' => 'dashboard'], function () {
  	// Auth Routes
    Route::middleware('guest')->group(function () {
        Route::get('login', 'AdminController@showLoginForm')->name('login');
        Route::post('login', 'AdminController@login');
    });
    // Authenticated Area Routes
    Route::middleware(['auth'])->group(function () {
      Route::get('logout', 'AdminController@logout')->name('logout');
      Route::get('/', 'AdminController@home')->name('admin_home');
      // Articles Routes
      Route::group(['prefix' => 'articles', 'middleware' => 'auth'], function () {
          Route::get('/published', 'ArticlesController@showAllArticles')->name('all_articles');
          Route::get('/add', 'ArticlesController@addArticle')->name('add_article');
          Route::post('/add', 'ArticlesController@handleAddArticle')->name('add_article_post');
          // Upload Inner Image
          Route::post('/upload_inner_image', 'ArticlesController@uploadInnerImage')->name('upload_inner_image');

      });

      // Featured Articles
      Route::get('featured_articles', 'FeaturedArticlesController@index')->name('featured_articles');
      Route::post('featured_articles', 'FeaturedArticlesController@handle')->name('handle_featured_articles');
    });

});
