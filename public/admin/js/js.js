function viewModal(url, title) {
    $.get(url, function(data) {
        $('#viewModalTitle').text(title);
        $('#viewModalContent').html(data);
        $('#viewModal').modal('show');
    })
}

function mainImageModal(imagesLink = '{{ route('main_image') }}') {
    $('#mainImageModal').modal('show');
        $('.main_image').attr('src', imagesLink);
}



function mainAlbumModal() {
    //$('#mainAlbumModal').modal('show');
    $('#mainAlbumModal').modal("show").on('hide', function() {
        $('#mainAlbumModal').modal('hide')
    });
}

function mainVideoModal() {
    $('#mainVideoModal').modal('show');
}

function deleteModal(url) {
    $('#deleteItemForm').attr('action', url);
    $('#deleteModal').modal('show');
}
$(document).ready(function() {
    $('#mainImageModal').on('hidden.bs.modal', function () {
    $('.main_image').attr('src', '');
})
});